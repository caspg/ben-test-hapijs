import { expect } from 'chai';
import glue from 'glue';
import mathRoutePlugin from '../../';

const logPlugin = {
  register: (server, options, next) => {
    server.expose('logger', () => {
      return () => {};
    });

    server.expose('addition', () => {});
    server.expose('substraction', () => {});
    server.expose('division', () => { throw new Error('from math-plugin'); });
    server.expose('multiplication', () => {});
    next();
  },
};

logPlugin.register.attributes = {
  name: 'log-plugin',
  version: '0.0.1',
};

const mathPlugin = {
  register: (server, options, next) => {
    server.expose('logger', () => {
      return () => {};
    });
    next();
  },
};

mathPlugin.register.attributes = {
  name: 'math-plugin',
  version: '0.0.1',
};

// This function starts up a HapiJS server with two plugins.
const setup = (callback) => {
  glue.compose({}, (err, server) => {
    server.register([
      {
        register: logPlugin,
        options: {},
      },
      {
        register: mathPlugin,
        options: {},
      },
      {
        register: mathRoutePlugin,
        options: {},
      }
    ], (registerErr) => {
      callback(registerErr, server);
    });
  });
};

describe('math-route-plugin', () => {
  it('registers the plugin', (done) => {
    setup((err) => {
      expect(err).to.equal(undefined);

      done();
    });
  });

  it('registers a new route called /calculate', (done) => {
    setup((err, server) => {
      const routingTable = server.table();

      expect(routingTable).to.have.length(1);
      expect(routingTable[0].table).to.have.length(1);
      expect(routingTable[0].table[0].path).to.equal('/calculate');

      done();
    });
  });

  it('defaults to op = add', (done) => {
    setup((err, server) => {
      server.inject('/calculate?x=2&y=3', (response) => {
        const { op } = response.request.query;

        expect(op).to.eql('add');

        done();
      });
    });
  })

  it('handles errors from math-plugin', (done) => {
    setup((err, server) => {
      server.inject('/calculate?x=2&y=3&op=divide', (response) => {
        expect(response.statusCode).to.equal(400);

        done();
      });
    });
  });

it('correctly handles missing x', (done) => {
    setup((err, server) => {
      server.inject('/calculate?y=3', (response) => {
        expect(response.statusCode).to.equal(400);

        done();
      });
    });
  });

  it('correctly handles missing y', (done) => {
    setup((err, server) => {
      server.inject('/calculate?x=3', (response) => {
        expect(response.statusCode).to.equal(400);

        done();
      });
    });
  });
});
