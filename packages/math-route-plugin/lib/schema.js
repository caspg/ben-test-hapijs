import Joi from 'joi';

/**
 * Build the schema object.
 *
 * @param {Object} server
 *   Server object.
 */
const buildSchema = (/* server */) => {
  const schema = {};

  schema.calculateValidate = {
    op: Joi.string().default('add').valid(['add', 'substract', 'divide', 'multiply']),
    x: Joi.number().required(),
    y: Joi.number().required(),
  };

  schema.calculateResponse = Joi.number();

  return schema;
};

module.exports = buildSchema;
