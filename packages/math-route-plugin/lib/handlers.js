import boom from 'boom';

const handlers = {};

/**
 * Calculate handler.
 *
 * @param {Object} request
 *   Request object.
 * @param {Function} reply
 *   Callback request.reply.
 */
handlers.calculate = (request, reply) => {
  const { log } = request.server.plugins['math-route-plugin'];
  const { op, x, y } = request.query;

  log('Calculate handler.');

  const operationMapper = {
    add: 'addition',
    substract: 'substraction',
    divide: 'division',
    multiply: 'multiplication',
  };

  const operationName = operationMapper[op];
  const operation = request.server.plugins['math-plugin'][operationName];

  try {
    const result = operation(x, y);

    return reply(result);
  } catch (error) {
    return reply(boom.badRequest(error));
  }
};

export default handlers;
