require('babel-core/register');

import pkg from './package.json';
import routesBuilder from './lib/routes';
import schemaBuilder from './lib/schema';

import handlers from './lib/handlers';

/**
 * Register plugin.
 *
 * @param {Object} server
 *   Server object.
 * @param {Object} options
 *   Plugin uptions.
 * @param {Function} next
 *   Callback expecting () or (err);
 */
exports.register = (server, options, next) => {
  const log = server.plugins['log-plugin'].logger('[PLUGIN EXAMPLE]');
  server.expose('log', log);

  log('Registered');

  const schema = schemaBuilder(server);
  const routes = routesBuilder(server, schema, handlers);

  server.route(routes);

  return next();
};

exports.register.attributes = {
  pkg,
  dependencies: [
    'log-plugin',
  ],
};
