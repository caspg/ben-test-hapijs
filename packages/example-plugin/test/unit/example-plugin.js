import { expect } from 'chai';
import glue from 'glue';
import examplePlugin from '../../';

// We need to mock the log-plugin plugin since example-plugin depends on it.
const logPlugin = {
  register: (server, options, next) => {
    server.expose('logger', () => {
      return () => {};
    });
    next();
  },
};

logPlugin.register.attributes = {
  name: 'log-plugin',
  version: '0.0.1',
};

// This function starts up a HapiJS server with two plugins.
const setup = (callback) => {
  glue.compose({}, (err, server) => {
    server.register([
      {
        register: logPlugin,
        options: {},
      },
      {
        register: examplePlugin,
        options: {},
      },
    ], (registerErr) => {
      callback(registerErr, server);
    });
  });
};

// Main test
describe('example-plugin', () => {
  it('registers the plugin', (done) => {
    setup((err) => {
      expect(err).to.equal(undefined);

      done();
    });
  });

  it('registers a new route called /hello', (done) => {
    setup((err, server) => {
      const routingTable = server.table();

      expect(routingTable).to.have.length(1);
      expect(routingTable[0].table).to.have.length(1);
      expect(routingTable[0].table[0].path).to.equal('/hello');

      done();
    });
  });

  it('defaults to n = 1', (done) => {
    setup((err, server) => {
      server.inject('/hello', (response) => {
        expect(response.result).to.deep.equal(['Item 1']);

        done();
      });
    });
  });

  it('correctly handles valid values of n', (done) => {
    const expectedResult = ['Item 1', 'Item 2', 'Item 3'];

    setup((err, server) => {
      server.inject('/hello?n=3', (response) => {
        expect(response.result).to.deep.equal(expectedResult);

        done();
      });
    });
  });

  it('correctly handles invalid 0 value', (done) => {
    setup((err, server) => {
      server.inject('/hello?n=0', (response) => {
        expect(response.statusCode).to.equal(400);

        done();
      });
    });
  });

    it('correctly handles invalid negative value', (done) => {
    setup((err, server) => {
      server.inject('/hello?n=-10', (response) => {
        expect(response.statusCode).to.equal(400);

        done();
      });
    });
  });
});
