import boom from 'boom';

const handlers = {};

/**
 * Hello handler.
 *
 * @param {Object} request
 *   Request object.
 * @param {Function} reply
 *   Callback request.reply.
 */
handlers.hello = (request, reply) => {
  const { log } = request.server.plugins['example-plugin'];
  const { n } = request.query;
  const response = [];

  log('Hello handler.');

  if (n < 1) {
    const err = new Error('The number must be greater than zero.');

    log(['error'], err);
    return reply(boom.badGateway(err));
  }

  for (let i = 1; i <= n; i += 1) {
    response.push(`Item ${i}`);
  }

  return reply(response);
};

export default handlers;
