import Joi from 'joi';

/**
 * Build the schema object.
 *
 * @param {Object} server
 *   Server object.
 */
const buildSchema = (/* server */) => {
  const schema = {};

  schema.helloValidate = {
    n: Joi.number().default(1).greater(0),
  };

  schema.helloResponse = Joi.array().items(Joi.string());

  return schema;
};

module.exports = buildSchema;
