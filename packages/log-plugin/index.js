require('babel-core/register');

import pkg from './package.json';
/**
 * Register plugin.
 *
 * @param {Object} server
 *   Server object.
 * @param {Object} options
 *   Plugin uptions.
 * @param {Function} next
 *   Callback expecting () or (err);
 */
exports.register = (server, options, next) => {
  console.log('[PLUGIN LOG]', 'Registered');

  server.expose('logger', (pluginName) => {
    return (...args) => console.log(pluginName, args);
  });

  return next();
};

exports.register.attributes = {
  pkg,
  dependencies: [],
};
