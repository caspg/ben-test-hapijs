/**
 * Addition function.
 *
 * @param {Number} x
 * @param {Number} y
 * @returns {Number}
 */
const addition = (x, y) => x + y;

/**
 * Substraction function.
 *
 * @param {Number} x
 * @param {Number} y
 * @returns {Number}
 */
const substraction = (x, y) => x - y;

/**
 * Multiplication function.
 *
 * @param {Number} x
 * @param {Number} y
 * @returns {Number}
 */

const multiplication = (x, y) => x * y;

/**
 * Division function.
 *
 * @param {Number} x - Numerator.
 * @param {Number} y - Denominator.
 * @returns {Number}
 */

const division = (x, y) => {
  if (y === 0) {
    const err = new Error('Denominator must be be greater than zero.');
    throw err;
  }

  return x / y;
};

module.exports = {
  addition,
  substraction,
  multiplication,
  division,
};
