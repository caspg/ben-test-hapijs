require('babel-core/register');

import pkg from './package.json';
import operations from './lib/operations';

/**
 * Register plugin.
 *
 * @param {Object} server
 *   Server object.
 * @param {Object} options
 *   Plugin uptions.
 * @param {Function} next
 *   Callback expecting () or (err);
 */
exports.register = (server, options, next) => {
  console.log('[PLUGIN MATH]', 'Registered');

  server.expose('addition', operations.addition);
  server.expose('substraction', operations.substraction);
  server.expose('multiplication', operations.multiplication);
  server.expose('division', operations.division);

  return next();
};

exports.register.attributes = {
  pkg,
  dependencies: [],
};
