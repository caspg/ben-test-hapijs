# Ben Coding Test - Server

This is a practical code test for the interview process at BEN.

## Setup

You're going to need NodeJS and [Lerna](https://lernajs.io/) installed globally.

* `npm install --global lerna@^2.0.0-beta.37`
* `cd path/to/project`
* `lerna bootstrap`
* `cd packages/server`
* `npm start` to start the application on port 3000: [http://localhost:3000](http://localhost:3000).
* `npm run eslint` to check the code.
* `cd packages/example-plugin` and `npm run test` to test the code and `npm run test:cover` to perform test coverage.

## What's with the odd structure? What's Lerna?

You might notice that the structure of the project is different from what you might be used with other NodeJS projects. This is what is called a `monorepo` allowing us to split large codebases into separately versioned packages. Having a main project that depends on other packages that we also develop means that we would normally have to symlink them into `node_packages` which can become tedious to mantain.

This is where Lerna comes into play, it handles the dependencies installations and the symlinking so that we don't have to do it. It also does other interesting stuff (taking care of package versions for example) but we don't use it for this particular project. You don't need to know Lerna, the installation instructions above should be all you need to get the project started, but if you want to know more this is a good tutorial: [reggi/lerna-tutorial](https://github.com/reggi/lerna-tutorial).

## What the project does

This is a small sample project that follows closely our code structure at BEN. It is a REST api that uses the [HapiJS](http://hapijs.com/) framework. It consists of a main application which uses plugins that add functionality to it, each plugin being a node package.

* `packages/server` - The main application. It is very lightweight, it just loads the configuration (from `manifest.json`), loads the plugins and boots up the server.
* `packages/example-plugin` - A simple plugin that creates a GET route at `/hello`, expecting a number as the `n` parameter. It returns a JSON array with `Item 1`, `Item 2`, etc based on the number sent. This is an example on how a plugin can register new routes. It also shows how you can use the [Joi](https://github.com/hapijs/joi) library to perform validation on what the server receives and what the server sends. It also contains a bit of test code showing how you can test a HapiJS plugin.
* `packages/log-plugin` - Another simple plugin that shows how you can expose functions or data to other plugins, that they can use. In this case, the plugin exposes a function that simply prints messages and it is being used by all the other plugins.

## Tasks

* Currently the `example-plugin` checks if the number sent is negative and throws an error if so. Joi allows us to set a range for numbers directly in the schema, so change the schema so that the allowed values for the `n` parameter are greater than 0.
* If you run test coverage for `example-plugin` you'll notice that code coverage is under 100% because we're not testing what happens when the number sent is negative or zero. Add a test to check for this inside `example-plugin/test/unit/example-plugin.js`.
* The final test in `example-plugin` checks whether the first element in the response contains the word `Item`. Change it so that it actually checks whether the response equals the full array (e.g. `['Item 1', 'Item 2', 'Item 3']` for `n = 3`).
* Create a new plugin, say `math-plugin`, that has a function or functions that perform addition, substraction, multiplication and division of two numbers.
* Create a new plugin that depends on `log-plugin` and `math-plugin` and that creates a new GET route, `/calculate`. The route handler should call function(s)  from the `math-plugin` and return the result. It should also handle errors coming from the math plugin (e.g. divide by zero attempted). The route schema should be:
  * Request:
    * `op` - `string`, can be `add`, `substract`, `divide`, `multiply`. Defaults to `add`.
    * `x` - `number`, is required.
    * `y` - `number`, is required.
  * Response:
    * `result` - `number`.

## Notes
* The initial code in `example-plugin` that checks for a negative number throws a `Bad Gateway` error. If you're implementing the test for this behaviour and you changed the code so that the validation is done by Joi then the error thrown might be a different one.
* How are dependencies created? Check `index.js` for `example-plugin` and look at the `exports.register.attributes` variable.
* The second plugin needs to communicate with the math plugin in some way. Use whatever method you are familiar with: Seneca microservices, if you're familiar with this library, or any other way that works in HapiJS (server methods, expose etc).
* Once you create a new plugin inside `packages`, you'll have to add it to the dependencies list of `server` and to the `manifest.json` file. But that is not enough, you'll have to rerun `lerna bootstrap` from the root directory so that the dependencies are installed and the new plugin is symlinked inside the server's `node_modules`.
* The purpose of this test is to see how comfortable you are with HapiJS and unit testing. Don't spend more than two hours on this (as we're not paying you for this we don't want you to waste too much time on it), if you're not done with it then send us what you have so far, non-working code is fine.
* We'd appreciate it if you would let us know your impression on the test. Was it too easy? Too difficult? What would you add or remove from it?

## Resources
* HapiJS tutorials: [http://hapijs.com/tutorials](http://hapijs.com/tutorials).
* The Joi API, for validation: [hapijs/joi](https://github.com/hapijs/joi).